package com.dbs.hack2hire.accountsmicroserviceserver.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TXN_HISTORY")
public class TransactionHistory implements Serializable {

	private static final long serialVersionUID = 2228075745410509745L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TXNID")
	private Long txnId;
	
	@Column(name = "CUSTOMERID")
	private String customerId;
	
	@Column(name = "ACCOUNTID")
	private String accountId;
	
	@Column(name = "TRANSACTIONID")
	private String transactionId;
	
	@Column(name = "TRANSACTIONTYPE")
	private String transactionType;
	
	@Column(name = "TRANSACTIONCODE")
	private String transactionCode;
	
	@Column(name = "TRANSACTIONAMOUNT")
	private String transactionAmount;
	
	@Column(name = "TRANSACTIONCATEGORY")
	private String transactioncategory;
	
	@Column(name = "DATETIME")
	private String dateTime;
	
	@Column(name = "REMARK")
	private String remark;

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {
		this.txnId = txnId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTransactioncategory() {
		return transactioncategory;
	}

	public void setTransactioncategory(String transactioncategory) {
		this.transactioncategory = transactioncategory;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "TransactionHistory [txnId=" + txnId + ", customerId=" + customerId + ", accountId=" + accountId
				+ ", transactionId=" + transactionId + ", transactionType=" + transactionType + ", transactionCode="
				+ transactionCode + ", transactionAmount=" + transactionAmount + ", transactioncategory="
				+ transactioncategory + ", dateTime=" + dateTime + ", remark=" + remark + "]";
	}
	
}
