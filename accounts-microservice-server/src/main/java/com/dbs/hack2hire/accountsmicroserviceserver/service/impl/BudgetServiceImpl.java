package com.dbs.hack2hire.accountsmicroserviceserver.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.dbs.hack2hire.accountsmicroserviceserver.model.AccountDetails;
import com.dbs.hack2hire.accountsmicroserviceserver.model.Budget;
import com.dbs.hack2hire.accountsmicroserviceserver.model.Product;
import com.dbs.hack2hire.accountsmicroserviceserver.model.TransactionHistory;
import com.dbs.hack2hire.accountsmicroserviceserver.repository.AccountReposity;
import com.dbs.hack2hire.accountsmicroserviceserver.repository.BudgetRepository;
import com.dbs.hack2hire.accountsmicroserviceserver.repository.TransactionHistoryRepository;
import com.dbs.hack2hire.accountsmicroserviceserver.service.BudgetService;
import com.dbs.hack2hire.accountsmicroserviceserver.util.SendEmail;

@Service
@Transactional
public class BudgetServiceImpl implements BudgetService {

	private static int txnid = 100001;

	private static final ExecutorService pool = Executors.newFixedThreadPool(20);

	@Autowired
	private BudgetRepository respository;

	@Autowired
	private TransactionHistoryRepository txnRepository;

	@Autowired
	private AccountReposity accountRepository;

	@Override
	public Budget setBudget(Budget budget) {
		System.out.println("setBudget no " + budget);
		Budget bud = respository.findByAccountNo(budget.getAccountNo());
		System.out.println("after repo call " + budget);
		if (bud != null) {
			budget.setbId(bud.getbId());
			return respository.save(budget);
		}
		return null;
	}

	@Override
	public Budget getBudget(String accountNo) {
		System.out.println("account no " + accountNo);
		return respository.findByAccountNo(accountNo);
	}

	@Override
	public String getFinancialAdvice(String accountno, String productname, String amount) {
		Budget budget = respository.findByAccountNo(accountno);

		double monthlyLimit = budget.getMonthlyExpensesLimit();
		double budgetLimit = budget.getMonthlyBudgetLimit();

		System.out.println("budget limit --->  " + budgetLimit + "    monthly limit ---> " + monthlyLimit);
		String advice = "Based on our AI algorithms running in our machines, we feel that this is not the correct time to buy this product";
		if (budgetLimit <= monthlyLimit + Double.valueOf(amount)) {
			advice = "based our ananlysis and on your profile, we suggest you that this is good time to buy this product, Njoy!!!";
		}
		return advice;

	}

	@Override
	public void createAccount(Budget buddget) {
		respository.save(buddget);
	}

	@Override
	public double buyProduct(Product product) {
		try {
			Budget budget = respository.findByAccountNo(product.getAccountno());
			budget.setMonthlyExpensesLimit(Math.abs(budget.getMonthlyExpensesLimit()) + product.getProductPrice());

			PageRequest request = PageRequest.of(0, 5, Sort.Direction.DESC, "txnId");
			List<TransactionHistory> txnList = this.txnRepository.findAllByCustomerId(product.getAccountno(), request);

			if (null != txnList && null != budget) {
				txnList.forEach(txn -> {
					if (null != txn.getTransactionAmount()) {
						double value = budget.getMonthlyExpensesLimit();
						value = value + Double.valueOf(txn.getTransactionAmount());
						budget.setMonthlyExpensesLimit(value);
					}
				});
			}

			this.makeTxnAudited(product); // make auditing for data analytics

			// update the existing balance.

			Optional<AccountDetails> details = null;

			if (product.getAccountno() != null && product.getAccountno().equalsIgnoreCase("123"))
				details = this.accountRepository.findById(1l);
			else
				details = this.accountRepository.findById(3l);

			if (details.isPresent()) {
				AccountDetails ad = details.get();
				ad.setBalance(ad.getBalance() - product.getProductPrice());
				this.accountRepository.save(details.get());
			}

			double remainingExpensesReport = (budget.getMonthlyBudgetLimit() - budget.getMonthlyExpensesLimit());
			// send the mail.
			if (remainingExpensesReport <= 0) {
				pool.execute(() -> {
					try {
						SendEmail.sendEmailNotification();
					} catch (Exception e) {
					}
					System.out.println("sent the mail sucessfully.");
				});
			}
			return remainingExpensesReport;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("unable to proceed with the transaction.");
		}
	}

	@Transactional
	private void makeTxnAudited(Product product) {
		TransactionHistory history = new TransactionHistory();
		history.setTransactionId(txnid + "");
		txnid++;
		history.setAccountId(product.getAccountno());
		history.setCustomerId(product.getAccountno());
		history.setDateTime(new Date().toString());
		history.setTransactionAmount(product.getProductPrice() + "");
		history.setTransactionType("DEBIT");
		history.setTransactionCode("CRDSWP");
		history.setRemark("AffordLah");
		history.setTransactioncategory("Utilities");
		txnRepository.save(history);
	}

	@Override
	public List<AccountDetails> getAccountDetails(String customerId) {
		return accountRepository.findAllByCustomerId(customerId); 
	}

	@Override
	public List<TransactionHistory> getTransactionHistory(String customerId) {
		return txnRepository.findAllByCustomerId(customerId, PageRequest.of(0, 2, Sort.Direction.DESC, "txnId"));
	}

}
