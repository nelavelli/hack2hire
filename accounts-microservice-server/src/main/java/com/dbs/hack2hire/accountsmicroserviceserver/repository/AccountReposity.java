package com.dbs.hack2hire.accountsmicroserviceserver.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dbs.hack2hire.accountsmicroserviceserver.model.AccountDetails;

@Repository
public interface AccountReposity extends CrudRepository<AccountDetails, Long> {
	
	List<AccountDetails> findAllByCustomerId(String customerId);

}
