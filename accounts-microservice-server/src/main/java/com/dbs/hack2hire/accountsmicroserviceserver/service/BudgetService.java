package com.dbs.hack2hire.accountsmicroserviceserver.service;

import java.util.List;

import com.dbs.hack2hire.accountsmicroserviceserver.model.AccountDetails;
import com.dbs.hack2hire.accountsmicroserviceserver.model.Budget;
import com.dbs.hack2hire.accountsmicroserviceserver.model.Product;
import com.dbs.hack2hire.accountsmicroserviceserver.model.TransactionHistory;

public interface BudgetService {
	
	void createAccount(Budget buddget);
	
	Budget setBudget(Budget budget);

	Budget getBudget(String accountNo);
	
	String getFinancialAdvice(String accountno,String productname,String amount);
	
	double buyProduct(Product product);	
	
	List<AccountDetails> getAccountDetails(String customerId);
	
	List<TransactionHistory> getTransactionHistory(String customerId);
}
