package com.dbs.hack2hire.accountsmicroserviceserver.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dbs.hack2hire.accountsmicroserviceserver.model.TransactionHistory;

@Repository
public interface TransactionHistoryRepository extends CrudRepository<TransactionHistory, Long> {

	List<TransactionHistory> findAllByCustomerId(String customerId, Pageable requset);
	
}
