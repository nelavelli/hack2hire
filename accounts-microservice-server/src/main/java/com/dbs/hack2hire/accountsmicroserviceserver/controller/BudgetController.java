package com.dbs.hack2hire.accountsmicroserviceserver.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.hack2hire.accountsmicroserviceserver.model.AccountDetails;
import com.dbs.hack2hire.accountsmicroserviceserver.model.Budget;
import com.dbs.hack2hire.accountsmicroserviceserver.model.Product;
import com.dbs.hack2hire.accountsmicroserviceserver.model.TransactionHistory;
import com.dbs.hack2hire.accountsmicroserviceserver.service.BudgetService;

@RestController
public class BudgetController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BudgetController.class);
	
	@Autowired
	private BudgetService budgetService;
	
	@PostMapping(value = "/createLimits", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> setLimits(@RequestBody Budget budget) {
		budgetService.setBudget(budget);
		return ResponseEntity.ok("Sucess");
	}

	@GetMapping(value = "/getLimits", produces = MediaType.APPLICATION_JSON_VALUE)
	public Budget getLimits(@RequestParam("accountNo") String accountNo) {

		Budget budget = budgetService.getBudget(accountNo);
		return budget;
	}

	@GetMapping("/advice")
	public ResponseEntity<String> getFinancialAdvice(@RequestParam("accountno") String accountno,
			@RequestParam("productname") String productname, @RequestParam("amount") String amount) {
		String advice = budgetService.getFinancialAdvice(accountno, productname, amount);

		return ResponseEntity.ok(advice);
	}

	@PostMapping(value = "/createAccount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createAccount(@RequestBody Budget budget) {
		budgetService.createAccount(budget);
		return ResponseEntity.ok("Account got created");
	}

	@PostMapping(value = "/buyProduct", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Double> buyProduct(@RequestBody Product product) {
		double remainingLimit = budgetService.buyProduct(product);
		return ResponseEntity.ok(remainingLimit);
	}
	
	@GetMapping("/getAccountDetails")
	public List<AccountDetails> getAccountDetails(@RequestParam("customerId") String customerId) {
		return budgetService.getAccountDetails(customerId);
	}
	
	@GetMapping("/getTxnHistory")
	public List<TransactionHistory> getTransactionHistory(String customerId) {
		return budgetService.getTransactionHistory(customerId);
	}
	
	
}
