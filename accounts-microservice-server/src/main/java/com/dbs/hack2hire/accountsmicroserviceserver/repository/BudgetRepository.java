package com.dbs.hack2hire.accountsmicroserviceserver.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dbs.hack2hire.accountsmicroserviceserver.model.Budget;

@Repository
public interface BudgetRepository extends CrudRepository<Budget, String> {

	Budget findByAccountNo(String accountNo);

}
