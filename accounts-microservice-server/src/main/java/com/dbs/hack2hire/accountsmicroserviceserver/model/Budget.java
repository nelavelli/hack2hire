package com.dbs.hack2hire.accountsmicroserviceserver.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BUDGET")
public class Budget implements Serializable {

	private static final long serialVersionUID = 605354286350313615L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    // @SequenceGenerator(name = "BUD_SYS_SEQ", sequenceName = "SEQ_BUDGET")
	@Column(name="BID")
	private Long bId;
	   
	@Column(name="ACCOUNTNO")   
	private String accountNo;

	@Column(name="MONTHLYINCOMELIMIT")   
	private double monthlyIncomeLimit;
	
	@Column(name="MONTHLYEXPENSELIMIT")   
	private double monthlyExpensesLimit;

	@Column(name="MONTHLYBUDGETLIMIT")   
	private double monthlyBudgetLimit;
	
	@Column(name="MONTHLYAVGBUDGETLIMIT")   
	private double monthlyAvgBudgetLimit;
	
	@Column(name="MONTHLYAVGINCOMELIMIT")   
	private double monthlyAvgIncomeLimit;
	
	@Column(name="MONTHLYAVGEXPENSELIMIT")   
	private double monthlyAvgExpenseLimit;
	
	@Column(name="BILL")
	private String bill;
	
	@Column(name="HOUSE")
	private String house;
	
	@Column(name="KIDANDFAMILY")
	private String kidandfamily;
	
	@Column(name="INSURANCE")
	private String insurance;

	@Column(name="TRAVEL")
	private String travel;
	
	@Column(name="OTHERS")
	private String others;

	public Long getbId() {
		return bId;
	}

	public void setbId(Long bId) {
		this.bId = bId;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public double getMonthlyIncomeLimit() {
		return monthlyIncomeLimit;
	}

	public void setMonthlyIncomeLimit(double monthlyIncomeLimit) {
		this.monthlyIncomeLimit = monthlyIncomeLimit;
	}

	public double getMonthlyExpensesLimit() {
		return monthlyExpensesLimit;
	}

	public void setMonthlyExpensesLimit(double monthlyExpensesLimit) {
		this.monthlyExpensesLimit = monthlyExpensesLimit;
	}

	public double getMonthlyBudgetLimit() {
		return monthlyBudgetLimit;
	}

	public void setMonthlyBudgetLimit(double monthlyBudgetLimit) {
		this.monthlyBudgetLimit = monthlyBudgetLimit;
	}

	public double getMonthlyAvgBudgetLimit() {
		return monthlyAvgBudgetLimit;
	}

	public void setMonthlyAvgBudgetLimit(double monthlyAvgBudgetLimit) {
		this.monthlyAvgBudgetLimit = monthlyAvgBudgetLimit;
	}

	public double getMonthlyAvgIncomeLimit() {
		return monthlyAvgIncomeLimit;
	}

	public void setMonthlyAvgIncomeLimit(double monthlyAvgIncomeLimit) {
		this.monthlyAvgIncomeLimit = monthlyAvgIncomeLimit;
	}

	public double getMonthlyAvgExpenseLimit() {
		return monthlyAvgExpenseLimit;
	}

	public void setMonthlyAvgExpenseLimit(double monthlyAvgExpenseLimit) {
		this.monthlyAvgExpenseLimit = monthlyAvgExpenseLimit;
	}

	public String getBill() {
		return bill;
	}

	public void setBill(String bill) {
		this.bill = bill;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getKidandfamily() {
		return kidandfamily;
	}

	public void setKidandfamily(String kidandfamily) {
		this.kidandfamily = kidandfamily;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getTravel() {
		return travel;
	}

	public void setTravel(String travel) {
		this.travel = travel;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	@Override
	public String toString() {
		return "Budget [bId=" + bId + ", accountNo=" + accountNo + ", monthlyIncomeLimit=" + monthlyIncomeLimit
				+ ", monthlyExpensesLimit=" + monthlyExpensesLimit + ", monthlyBudgetLimit=" + monthlyBudgetLimit
				+ ", monthlyAvgBudgetLimit=" + monthlyAvgBudgetLimit + ", monthlyAvgIncomeLimit="
				+ monthlyAvgIncomeLimit + ", monthlyAvgExpenseLimit=" + monthlyAvgExpenseLimit + ", bill=" + bill
				+ ", house=" + house + ", kidandfamily=" + kidandfamily + ", insurance=" + insurance + ", travel="
				+ travel + ", others=" + others + "]";
	}


}
